# Cummill

Based on Postmill. Built on the [Symfony](https://symfony.com/) framework.

## Contributions

You are always welcome to submit merge requests for bug fixes, improvements,
documentation, translations, and so on.

If you want to contribute a new feature, you should start an issue on the issue
tracker to gauge the possibility of it being merged. A feature that's outside
the scope of the software is likely to be rejected. 

## License

The software is released under the zlib license. See the `LICENSE` file for
details.
