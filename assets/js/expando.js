var imgs = document.querySelectorAll('.comment__body img, .submission__body img, .submission__nav img');
var links = document.querySelectorAll('.comment__body a, .submission__body a');
var expOpenUrl = 'data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyMyIgaGVpZ2h0PSIyMyIgdmlld0JveD0iMCAwIDIzIDIzIj4KCTxyZWN0IHg9IjAiIHk9IjAiIHdpZHRoPSIyMyIgaGVpZ2h0PSIyMyIgcng9IjIuNSIgcnk9IjIuNSIgZmlsbD0icmdiKDIwNCwgMjA0LCAyMDEpIiAvPgoJPGcgZmlsbD0id2hpdGUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+CgkJPHBhdGgKCQkJZD0iCgkJCQlNNCw1IGwxLC0uNSBoMiBsMSwuNSBoMSBsMSwtMSBoMyBsMiwyIGgyCgkJCQl2OCBoLTEzegoJCQkJTTUsNiBoMiB2MSBoLTIgdi0xegoJCQkJTSAxMS41IDkuNSBtIC0zLjUsIDAgYSAzLjUsMy41IDAgMSwwIDcsMCBhIDMuNSwzLjUgMCAxLDAgLTcsMAoJCQkJTSAxMS41IDkuNSBtIC0yLjUsIDAgYSAyLjUsMi41IDAgMSwwIDUsMCBhIDIuNSwyLjUgMCAxLDAgLTUsMAoJCQkiCgkJLz4KCTwvZz4KCTxnIHN0cm9rZT0id2hpdGUiIHN0cm9rZS13aWR0aD0iMS41IiBzdHJva2UtbGluZWNhcD0icm91bmQiPgoJCTxsaW5lIHgxPSIxOC43NSIgeTE9IjE2LjUiIHgyPSIxOC43NSIgeTI9IjIxIiAvPgoJCTxsaW5lIHgxPSIxNi41IiB5MT0iMTguNzUiIHgyPSIyMSIgeTI9IjE4Ljc1IiAvPgoJPC9nPgo8L3N2Zz4=';
var expCloseUrl = 'data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyMyIgaGVpZ2h0PSIyMyIgdmlld0JveD0iMCAwIDIzIDIzIj4KCTxjaXJjbGUgY3g9IjExLjUiIGN5PSIxMS41IiByPSIxMS41IiBmaWxsPSJyZ2IoMjA0LCAyMDQsIDIwMSkiIC8+Cgk8ZwoJCXN0cm9rZT0id2hpdGUiCgkJc3Ryb2tlLXdpZHRoPSIzLjI1IgoJCXN0cm9rZS1saW5lY2FwPSJyb3VuZCIKCT4KCQk8bGluZSB4MT0iNy41IiB5MT0iNy41IiB4Mj0iMTUuNSIgeTI9IjE1LjUiIC8+CgkJPGxpbmUgeDE9IjE1LjUiIHkxPSI3LjUiIHgyPSI3LjUiIHkyPSIxNS41IiAvPgoJPC9nPgo8L3N2Zz4=';

function imgurApi(action) {
    const base = 'https://api.imgur.com/3/';
    const apiId = '1d8d9b36339e0e2';
    let headers = {
        'Authorization': 'Client-ID ' + apiId,
        'Content-Type': 'application/json',
    }

    return fetch(base + action, {
        headers: headers
    })
    .then(res => res.json())
    .then(data => data.data)
}

var linkParsers = {
    img: {
        detect: (url) => (/\.(gif|jpe?g|png|svg)$/i).exec(url),
        setUrl: (url, groups, img) => img.src = url
    },
    imgurGeneric: {
        detect: (url) => (/^https?:\/\/(?:i\.|m\.|edge\.|www\.)*imgur\.com\/(r\/\w+\/)*(?!gallery)(?!removalrequest)(?!random)(?!memegen)((?:\w{5}|\w{7})(?:[&,](?:\w{5}|\w{7}))*)(?:#\d+)?[a-z]?(\.(?:jpe?g|gifv?|png))?(\?.*)?$/i).exec(url),
        setUrl: function(url, groups, img) {
            imgurApi('image/' + groups[2])
            .then(data => img.src = data.link)
        }
    },
    imgurAlbum: {
        detect: (url) => (/^https?:\/\/(?:m\.|www\.)?imgur\.com\/a\/(\w+)(?:[/#]|$)/i).exec(url),
        setUrl: function(url, groups, img) {
            imgurApi('album/' + groups[1])
            .then(data => img.src = data.images[0].link)
        }
    },
    imgurGallery: {
        detect: (url) => (/^https?:\/\/(?:m\.|www\.)?imgur\.com\/gallery\/(\w+)(?:[/#]|$)/i).exec(url),
        setUrl: function(url, groups, img) {
            imgurApi('gallery/' + groups[1])
            .then(data => img.src = data.images[0].link)
        }
    }
}
window.imgurApi = imgurApi;
window.linkParsers = linkParsers;

function toggleImage(e) {
	var img = document.getElementById(e.target.getAttribute('image-id')).querySelector('img.subexpando');
    window.testme = img;

	if (img.style.getPropertyValue('display') == 'none') {
		e.target.src = expCloseUrl;
        img.style.setProperty('display', 'block');
	}
	else {
		e.target.src = expOpenUrl;
        img.style.setProperty('display', 'none');
	}
}

function startDrag(event) {
    let targetImg = event.target;
    let startX = targetImg.x;
    let startY = targetImg.y;
    let startD = Math.round(Math.hypot(event.pageX - startX, event.pageY - startY));
    let startW = targetImg.width;
    let startH = targetImg.height;
    let ratio = startW / startH;
    let lastDist = 0;

    function stopDrag() {
        document.removeEventListener('mousemove', onMouseMove);
        document.removeEventListener('mouseup', stopDrag);

        // check to see if it was just a click and not a real drag, if so open it
        if (event.button == 0 && lastDist < 2) {
            window.open(targetImg.src);
        }
    }

    function onMouseMove(event) {
        let dist = Math.round(Math.hypot(event.pageX - startX, event.pageY - startY));
        lastDist = dist;
        let beforeW = targetImg.width;
        targetImg.width = startW + (dist - startD) * ratio;
        // check whether the width changed and only tweak height if it did. this is to prevent
        // continuing to scale one dimension when the other is locked due to hitting its max
        if (targetImg.width != beforeW) {
            targetImg.height = targetImg.width / ratio;
        }
    }

    document.addEventListener('mousemove', onMouseMove);
    document.addEventListener('mouseup', stopDrag);
}

for (var i = 0; i < imgs.length; i++) {
	var imgWrapper = document.createElement('span');
	var wrappedImg = document.createElement('img');
    var wrappedImgP = document.createElement('p');
	var imgToggle = document.createElement('img');
	var imgLink = document.createElement('a');

	wrappedImg.src = imgs[i].src;
	wrappedImg.setAttribute('alt', imgs[i].getAttribute('alt'));
    wrappedImg.onmousedown = startDrag;
    wrappedImg.ondragstart = function() { return false; }
    wrappedImg.style.setProperty('display', 'none');
    wrappedImg.style.setProperty('max-width', '100%');
    wrappedImg.style.setProperty('max-height', '100%');
    wrappedImg.classList.add('subexpando');
    wrappedImgP.appendChild(wrappedImg);
	wrappedImgP.setAttribute('id', 'image-' + i);

	imgToggle.src = expOpenUrl;
	imgToggle.style.setProperty('margin-left', '0.5em');
	imgToggle.setAttribute('image-id', 'image-' + i);
	imgToggle.classList.add('img-toggle');
	imgToggle.onclick = toggleImage;

	imgLink.href = imgs[i].src;
	imgLink.innerHTML = imgs[i].getAttribute('alt');
	imgLink.setAttribute('target', '_blank');

	imgWrapper.appendChild(imgLink);
    // expandos for submission lists look a bit different. for those, we put the
    // button in a <span> rather than in-place
    var exSpan = imgs[i].parentNode.querySelector('span');
    if (exSpan) {
        exSpan.appendChild(imgToggle);
    }
    else {
        imgWrapper.appendChild(imgToggle);
    }
	imgWrapper.appendChild(wrappedImgP);

	imgs[i].parentNode.insertBefore(imgWrapper, imgs[i]);
	imgs[i].remove();
}

for (var i = 0; i < links.length; i++) {
    let groups;

    for (var parser in linkParsers) {
        if ((groups = linkParsers[parser].detect(links[i].href))) {
            var imgWrapper = document.createElement('span');
            var wrappedImg = document.createElement('img');
            var imgToggle = document.createElement('img');

            wrappedImg.onmousedown = startDrag;
            wrappedImg.ondragstart = function() { return false; }
            wrappedImg.style.setProperty('display', 'none');
            wrappedImg.style.setProperty('max-width', '100%');
            wrappedImg.style.setProperty('max-height', '100%');
            wrappedImg.classList.add('subexpando');
            linkParsers[parser].setUrl(links[i].href, groups, wrappedImg);

            imgToggle.src = expOpenUrl;
            imgToggle.style.setProperty('margin-left', '0.5em');
            imgToggle.setAttribute('image-id', 'image-' + (imgs.length + i));
            imgToggle.classList.add('img-toggle');
            imgToggle.onclick = toggleImage;

            imgWrapper.setAttribute('id', 'image-' + (imgs.length + i));
            imgWrapper.appendChild(imgToggle);
            imgWrapper.appendChild(wrappedImg);

            links[i].parentNode.insertBefore(imgWrapper, links[i].nextSibling);
            break;
        }
    }
}
