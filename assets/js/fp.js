const Fingerprint2 = require('fingerprintjs2');

export function fpFunc() {
    if (!window.location.href.endsWith('/login')) {
        return;
    }
    Fingerprint2.get(function (components) {
        const fpval = Fingerprint2.x64hash128(components.map(function (component) {
            return component.value;
        }).join(''), 31);
        document.getElementsByName('user_session')[0].value = fpval;
    });
}

if (window.requestIdleCallback) {
    requestIdleCallback(function () {
        fpFunc();
    });
} else {
    setTimeout(function () {
        fpFunc();
    }, 500);
}

document.getElementById('login-username').onfocus = function () {
    let charmer = document.getElementById('charmer');
    if (charmer != null) {
        charmer.play();
    }
}
