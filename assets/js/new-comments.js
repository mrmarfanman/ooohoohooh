function handleNewComments() {
    var nav = document.querySelector('nav.submission__nav');
    if (!nav) {
        return;
    }
    var navLi = nav.querySelector('li');
    if (!navLi) {
        return;
    }
    var subTokens = navLi.children[0].href.split('/');
    var subId = subTokens[subTokens.length - 2];

    var maxTimestamp = localStorage.getItem('latestComment-' + subId);
    var submissionComments = document.querySelectorAll('div.comment__row');

    if (!submissionComments) {
        return;
    }

    if (maxTimestamp && !document.querySelector('div.single-comment-alert')) {
        for (var i = 0; i < submissionComments.length; i++) {
            var cTime = submissionComments[i].querySelector('time').dateTime;
            if (cTime > maxTimestamp) {
                submissionComments[i].classList.add('new-comment');
            }
        }
    }
    else {
        maxTimestamp = '1969-04-20T00:00:00+00:00';
    }

    var commentTimestamps = document.querySelectorAll('h1.comment__info time');

    for (i = 0; i < commentTimestamps.length; i++) {
        if (commentTimestamps[i].dateTime >= maxTimestamp) {
            maxTimestamp = commentTimestamps[i].dateTime;
        }
    }

    localStorage.setItem('latestComment-' + subId, maxTimestamp);
}

handleNewComments();
