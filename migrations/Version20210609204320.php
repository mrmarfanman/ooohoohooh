<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210609204320 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Add sortnew column to submissions';
    }

    public function up(Schema $schema) : void
    {
        //$this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');
        //$this->addSql('ALTER TABLE submissions ADD sortnew BOOLEAN DEFAULT FALSE');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE submissions DROP sortnew');
    }
}
