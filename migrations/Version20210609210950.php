<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210609210950 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Adds fields for submission favorites';
    }

    public function up(Schema $schema) : void
    {
        //$this->addSql('CREATE SEQUENCE submission_favorites_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        //$this->addSql('CREATE TABLE submission_favorites (id BIGINT NOT NULL, user_id BIGINT NOT NULL, submission_id BIGINT NOT NULL, ip inet NOT NULL, timestamp TIMESTAMP(0) WITH TIME ZONE NOT NULL, PRIMARY KEY(id))');
        //$this->addSql('COMMENT ON COLUMN submission_favorites.ip IS \'(DC2Type:inet)\'');
        //$this->addSql('CREATE INDEX IDX_SFYOURMOMCOOL395 ON submission_favorites (user_id)');
        //$this->addSql('CREATE INDEX IDX_SFNOREALLYDUDE95 ON submission_favorites (submission_id)');
        //$this->addSql('CREATE UNIQUE INDEX submission_user_favorite_idx ON submission_favorites (submission_id, user_id)');
        //$this->addSql('ALTER TABLE submission_favorites ADD CONSTRAINT FK_SFUIHOPEYOUENJOY FOREIGN KEY (user_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        //$this->addSql('ALTER TABLE submission_favorites ADD CONSTRAINT FK_THESEGREATFKNAME FOREIGN KEY (submission_id) REFERENCES submissions (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE submission_favorites DROP CONSTRAINT FK_THESEGREATFKNAME');
        $this->addSql('ALTER TABLE submission_favorites DROP CONSTRAINT FK_SFUIHOPEYOUENJOY');
        $this->addSql('DROP SEQUENCE submission_favorites_id_seq CASCADE');
        $this->addSql('DROP TABLE submission_favorites');
    }
}
