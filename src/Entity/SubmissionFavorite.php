<?php

namespace App\Entity;

use App\Repository\SubmissionFavoriteRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SubmissionFavorite")
 * @ORM\Table(uniqueConstraints={
 *     @ORM\UniqueConstraint(
 *         name="submission_user_favorite_idx",
 *         columns={"submission_id", "user_id"}
 *     )
 * })
 * @ORM\AssociationOverrides({
 *     @ORM\AssociationOverride(name="user", inversedBy="submissionFavorites")
 * })
 */
class SubmissionFavorite
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\JoinColumn(name="user_id", nullable=false)
     * @ORM\ManyToOne(targetEntity="User")
     *
     * @var User
     */
    private $user;

    /**
     * @ORM\JoinColumn(name="submission_id", nullable=false)
     * @ORM\ManyToOne(targetEntity="Submission", inversedBy="favorites")
     *
     * @var Submission
     */
    private $submission;

    /**
     * @ORM\Column(type="inet", nullable=true)
     */
    private $ip;

    /**
     * @ORM\Column(type="datetimetz_immutable")
     */
    private $timestamp;

    public function __construct(User $user, ?string $ip, Submission $submission) {
        $this->user = $user;
        $this->submission = $submission;
        $this->setIp($ip);
        $this->timestamp = new \DateTimeImmutable('@'.time());

        $submission->addFavorite($this);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): User {
        return $this->user;
    }

    public function getSubmission(): Submission {
        return $this->submission;
    }

    public function getIp()
    {
        return $this->ip;
    }

    public function setIp($ip): self
    {
        $this->ip = $ip;

        return $this;
    }

    public function getTimestamp(): ?\DateTimeImmutable
    {
        return $this->timestamp;
    }

    public function setTimestamp(\DateTimeImmutable $timestamp): self
    {
        $this->timestamp = $timestamp;

        return $this;
    }
}
