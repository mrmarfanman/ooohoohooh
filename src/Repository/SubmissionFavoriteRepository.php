<?php

namespace App\Repository;

use App\Entity\SubmissionFavorite;
use App\Repository\Contracts\PrunesIpAddresses;
use App\Repository\Traits\PrunesIpAddressesTrait;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SubmissionFavorite|null find($id, $lockMode = null, $lockVersion = null)
 * @method SubmissionFavorite|null findOneBy(array $criteria, array $orderBy = null)
 * @method SubmissionFavorite[]    findAll()
 * @method SubmissionFavorite[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SubmissionFavoriteRepository extends ServiceEntityRepository implements PrunesIpAddresses {
    use PrunesIpAddressesTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SubmissionFavorite::class);
    }
}
