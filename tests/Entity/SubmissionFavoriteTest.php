<?php

namespace App\Tests\Entity;

use App\Entity\SubmissionFavorite;
use App\Tests\Fixtures\Factory\EntityFactory;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\Entity\SubmissionFavorite
 */
class SubmissionFavoriteTest extends TestCase {
    public function testGetSubmission(): void {
        $submission = EntityFactory::makeSubmission();
        $Favorite = new SubmissionFavorite(
            EntityFactory::makeUser(),
            null,
            $submission,
        );

        $this->assertSame($submission, $Favorite->getSubmission());
    }

    public function testSubmissionHasFavoriteAfterInit(): void {
        $submission = EntityFactory::makeSubmission();
        $user = EntityFactory::makeUser();
        $Favorite = new SubmissionFavorite($user, null, $submission);

        $this->assertSame($Favorite, $submission->getUserFavorite($user));
    }
}
